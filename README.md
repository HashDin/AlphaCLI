# #D: CLI Alpha Client

This repo will be the reference client implementation of the #D spec for blockchain generation and cross-signing of #D identity chains. 


This is intended to be our test platform for the simplest implementation that will allow for rudimentary reputation systems to be built on top of the primitives provided by it.
The goal is that this tool will be powerful enough to help organize developers and become the backbone to the The Everything Co.(TEC), the organization funding the development of this protocol. 


### Functionality


- [ ] Generate Valid Blocks 
- [ ] Send Blocks to #D miners
- [ ] Connect to other peers
- [ ] Share and store block headers from other nodes
- [ ] Subscribe to topics from other nodes
- [ ] Send and respond to queries from other nodes
- [ ] Simple BBS style message board built in
